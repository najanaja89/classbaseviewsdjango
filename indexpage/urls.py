from django.contrib import admin
from django.urls import path

from . import views

app_name = 'indexpage'

urlpatterns = [
    # path('', views.indexpage)
    path('', views.IndexPageView.as_view()),
]
