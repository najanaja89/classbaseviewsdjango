from django.shortcuts import render, HttpResponse
from django.views.generic import View, TemplateView


# Create your views here.
# def indexpage(request):
#     return render(request, 'indexpage/index.html')

# class IndexPageView(View):
#     template_name = "index.html"
#
#     def get(self, request):
#         return render(request, template_name=self.template_name)
#
#     def post(self, request):
#         return HttpResponse('post is ok')


class IndexPageView(TemplateView):
    template_name = 'indexpage/index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                'foo': 'bar'
            }
        )
        return context
